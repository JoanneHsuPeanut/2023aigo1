import PySimpleGUI as sg
import openai

# 讀取 OpenAI API 金鑰
with open("openaiKey.txt", "r") as file:
    openaiKey = file.read().strip()

# 設置 OpenAI API 金鑰
openai.api_key = openaiKey

# 呼叫 OpenAI API 生成中文結論
def generate_summary():
    # 讀取英文文稿
    with open("input.txt", "r") as file:
        text = file.read()

    # 使用 OpenAI API 進行生成
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=f"提供以下文字之500字繁體中文摘要：{text}",
        max_tokens=1000,
        temperature=0.7,
        n=1,
        stop=None,
        api_key=openaiKey
    )

    # 提取生成的中文結論
    result = response.choices[0].text.strip()
    print(result)
    print()

    # 將中文結論儲存到 output.txt 檔案
    with open("output.txt", "w", encoding="utf-8") as file:
        file.write(result)

# 建立 GUI 視窗
layout = [
    [sg.Button("生成中文結論", key="-SUMMARY-")],
    [sg.Output(size=(60, 20))],
]

window = sg.Window("OpenAI中文摘要小幫手", layout)

# 主迴圈
while True:
    event, values = window.read()
    if event == sg.WINDOW_CLOSED:
        break
    elif event == "-SUMMARY-":
        generate_summary()
        print("中文結論已生成，儲存到 output.txt")
    

