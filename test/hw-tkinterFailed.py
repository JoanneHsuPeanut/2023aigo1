import tkinter as tk
import openai


# 設定 OpenAI API 金鑰
# api_key = "你的 API 金鑰"
# openai.api_key = api_key

# 定義 GUI 視窗
window = tk.Tk()
window.title("OpenAI Whisper 模型")
window.geometry("400x200")

# 定義視窗元素的回呼函式
def generate_summary():
    # 讀取輸入的英文文稿
    with open("input.txt", "r") as file:
        text = file.read()

    # 使用 OpenAI API 進行生成
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=text,
        max_tokens=100,
        temperature=0.7,
        n=1,
        stop=None,
        model="gpt-3.5-turbo",
    )

    # 提取生成的中文結論
    summary = response.choices[0].text.strip()

    # 將中文結論顯示在視窗上
    output_text.delete("1.0", tk.END)
    output_text.insert(tk.END, summary)

# 輸入檔案按鈕
input_button = tk.Button(window, text="選擇英文文稿", command=generate_summary, foreground="black")
input_button.pack()

# 中文結論輸出
output_text = tk.Text(window, height=10, width=40)
output_text.pack()

# 啟動 GUI 主迴圈
window.mainloop()
