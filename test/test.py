import openai

# 讀取 OpenAI API 金鑰
with open("openaiKey.txt", "r") as file:
    openaiKey = file.read().strip()

# 設置 OpenAI API 金鑰
openai.api_key = openaiKey


with open('input.txt', 'r') as fh:
    tmp = fh.read()
    itemlist = tmp.split(',')

itemlist = str(itemlist)

start_idx = 0
result = ''
while start_idx < len(itemlist):
    end_idx = min(start_idx + 1600, len(itemlist))
    sub_list = itemlist[start_idx:end_idx]

    print(start_idx)

    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a chatbot"},
            {"role": "user", "content": f"提供以下文字之50字繁體中文摘要：{sub_list}"}
        ]
    )  
    for choice in response.choices:
        result += choice.message.content
    start_idx = end_idx

with open('output.txt', 'w', encoding='utf-8') as output_file:
    output_file.write(result)