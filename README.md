課名：2023-數位發展部-AIGO1-OpenAI API 之自然語言處理入門與實務應用  
授課教師：鄭伯壎 教授  
姓名：許嘉恩  

## 專題作業 OpenAI英文文本中文摘要
- main
  - 讀取與設置 OpenAI API 金鑰
  - 建立 GUI 視窗與視窗主迴圈
    - 按下「生成中文結論」按鈕：呼叫 ```generate_summary()```
    - 關閉視窗：break
- sub：```generate_summary()```  
  - 讀取英文文稿 [input.txt](https://gitlab.com/JoanneHsuPeanut/2023aigo1/-/blob/master/input.txt) 到 ```text```  
  - 呼叫OpenAI API，使用：```prompt=f"提供以下文字之500字繁體中文摘要：{text}"``` 取得500字中文結論  
  - 在視窗印出中文結論，並輸出至 [output.txt](https://gitlab.com/JoanneHsuPeanut/2023aigo1/-/blob/master/output.txt)  

#### GUI截圖  
<img src="作業成功截圖.png"
title= “hw-joanne-aigo1” width="40%">

> 註：原本嘗試用tkinter寫GUI，但在我的電腦上按鈕的字和底色都是白色的，也無法改變顏色，故改用PySimpleGUI

#### 未來展望  
1. 用drag and drop的方式匯入檔案，操作更直覺  
1. 自訂摘要字數的按鈕
1. 偵測語言、自訂摘要語言，化為語言學習工具

---
#### [hw-final.py](https://gitlab.com/JoanneHsuPeanut/2023aigo1/-/blob/master/hw-final.py) 程式碼
``` python
import PySimpleGUI as sg
import openai

# 讀取 OpenAI API 金鑰
with open("openaiKey.txt", "r") as file:
    openaiKey = file.read().strip()

# 設置 OpenAI API 金鑰
openai.api_key = openaiKey

# 呼叫 OpenAI API 生成中文結論
def generate_summary():
    # 讀取英文文稿
    with open("input.txt", "r") as file:
        text = file.read()

    # 使用 OpenAI API 進行生成
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=f"提供以下文字之500字繁體中文摘要：{text}",
        max_tokens=1000,
        temperature=0.7,
        n=1,
        stop=None,
        api_key=openaiKey
    )

    # 提取生成的中文結論
    result = response.choices[0].text.strip()
    print(result)
    print()

    # 將中文結論儲存到 output.txt 檔案
    with open("output.txt", "w", encoding="utf-8") as file:
        file.write(result)

# 建立 GUI 視窗
layout = [
    [sg.Button("生成中文結論", key="-SUMMARY-")],
    [sg.Output(size=(60, 20))],
]

window = sg.Window("OpenAI中文摘要小幫手", layout)

# 主迴圈
while True:
    event, values = window.read()
    if event == sg.WINDOW_CLOSED:
        break
    elif event == "-SUMMARY-":
        generate_summary()
        print("中文結論已生成，儲存到 output.txt")

```
