import openai

# 讀取 OpenAI API 金鑰
with open("openaiKey.txt", "r") as file:
    openaiKey = file.read().strip()

# 設置 OpenAI API 金鑰
openai.api_key = openaiKey

# 讀取英文文稿
with open("input.txt", "r") as file:
    text = file.read()

result = ''

# 使用 OpenAI API 進行生成
response = openai.Completion.create(
    model="gpt-3.5-turbo",
    messages=[
        {"role": "system", "content": "You are a chatbot"},
        {"role": "user", "content": f"提供以下文字之80字繁體中文摘要：{text}"}
    ]
)

# 提取生成的中文結論
for choice in response.choices:
    result += choice.message.content

print(result)