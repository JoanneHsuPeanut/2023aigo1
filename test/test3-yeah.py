import openai

with open("input.txt", "r") as file:
    text = file.read()

# 讀取 OpenAI API 金鑰
with open("openaiKey.txt", "r") as file:
    openaiKey = file.read().strip()

# 使用 OpenAI API 進行生成
response = openai.Completion.create(
    engine="text-davinci-003",
    prompt=f"提供以下文字之500字繁體中文摘要：{text}",
    max_tokens=1000,
    temperature=0.7,
    n=1,
    stop=None,
    api_key=openaiKey
)


# 提取生成的中文結論
result = response.choices[0].text.strip()

print(result)
